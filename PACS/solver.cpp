// ----------------------------------------------------------------------------
/**
   File: solver.cpp

   Description: Class containing the main solver of the heuristic. The solver is
                responsible for maintaining updated representations of all the 
                optimization models, as well as finding improvements in feasibility 
                and optimality.
   Created by Lluis-Miquel Munguia Conejero (lluis.munguia@gatech.edu)
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/ 
// ----------------------------------------------------------------------------
#include "solver.h"

//Utility functions
bool boundVectorSort (pair <int, long long int> a, pair <int, long long int> b) { 
  return (a.second<b.second); 
}
inline bool testInteger(double x){
  int ceiling=ceil(x);
  int flr=floor(x);
  return (x-flr<EPSILON) || (ceiling-x<EPSILON);
}

inline int roundToNearestInteger(double x){
  int ceiling=ceil(x);
  int flr=floor(x);
  if (x-flr<0.5) return flr;
  return ceiling;
}

double randomNumber(double lb, double ub, char type){
  int range = ub-lb+1;
  if (type=='C'){
    double r = ((double)rand() / (double)(RAND_MAX));//this is a number between 0 and 1
    double number = lb + r*range;
    return number;
  }
  else {
    int r = rand() % range;
    return lb+r;
  }
}

//construction and initialization
solver::solver ()
{
 
}

solver::~solver ()
{
  if (integerVariableIndices)delete integerVariableIndices;
  CPXcloseCPLEX (&env);

}

int solver::initSolver(string &s, int thread,double &_TOTAL_CONSTRS_COVER,int &_INITIAL_TILIM, double &_TOTAL_CONSTRS_COVER_OBJ,int &_INITIAL_TILIM_OBJ,double &_DELTA_TOLERANCE, double &_INIT_FIXING_COVER, int &NTHREADS, int &_nproc){

  int status;
  NUM_THREADS=NTHREADS;
  nproc=_nproc;
  env = CPXopenCPLEX (&status);
  lp = CPXcreateprob (env, &status, s.c_str());
  if (status!=0)return 0;
  status = CPXreadcopyprob (env, lp, s.c_str(), NULL);
  if (status!=0)return 0;
  originalLp= CPXcloneprob (env, lp, &status);
  if (status!=0)return 0;

  int numCols=CPXgetnumcols(env,lp);
  int numRows= CPXgetnumrows(env,lp);

  if (_TOTAL_CONSTRS_COVER==-1){
    if (numCols<pow(10,4) &&numRows<pow(10,4)){
      _TOTAL_CONSTRS_COVER=0.1;
    }
    else if (numCols<pow(10,5) &&numRows<pow(10,5)){
      _TOTAL_CONSTRS_COVER=0.3;
    }
    else if (numCols<pow(10,6) &&numRows<pow(10,6)){
      _TOTAL_CONSTRS_COVER=0.5;
    }
    else if (numCols<pow(10,7) &&numRows<pow(10,7)){
      _TOTAL_CONSTRS_COVER=0.8;
    }
    else _TOTAL_CONSTRS_COVER=0.95;
    _TOTAL_CONSTRS_COVER_OBJ=_TOTAL_CONSTRS_COVER;
  }

  if (_INITIAL_TILIM==-1){
    if (numCols<pow(10,4) &&numRows<pow(10,4)){
      _INITIAL_TILIM=5;
    }
    else if (numCols<pow(10,5) &&numRows<pow(10,5)){
      _INITIAL_TILIM=10;
    }
    else if (numCols<pow(10,6) &&numRows<pow(10,6)){
      _INITIAL_TILIM=20;
    }
    else if (numCols<pow(10,7) &&numRows<pow(10,7)){
      _INITIAL_TILIM=50;
    }
    else _TOTAL_CONSTRS_COVER=100;
    _INITIAL_TILIM_OBJ=_INITIAL_TILIM;
  }

  if (_INIT_FIXING_COVER==-1){
    _INIT_FIXING_COVER=_TOTAL_CONSTRS_COVER;
  }

  if (_DELTA_TOLERANCE==-1)_DELTA_TOLERANCE=0;

  TOTAL_CONSTRS_COVER=_TOTAL_CONSTRS_COVER;
  TOTAL_CONSTRS_COVER_OBJ=_TOTAL_CONSTRS_COVER;
  INITIAL_TILIM=_INITIAL_TILIM;
  CPLEXTHREADS=omp_get_num_procs()/(2*NTHREADS);
  DELTA_TOLERANCE=_DELTA_TOLERANCE;
  INIT_FIXING_COVER=_INIT_FIXING_COVER;

  if (PRINTOUT && nproc==0)status = CPXwriteprob (env, lp, "originalMIP.lp", NULL);

  tilimObj=_INITIAL_TILIM_OBJ;
  tilim=_INITIAL_TILIM;
  TOTAL_CONSTRS_COVER_OBJ=_TOTAL_CONSTRS_COVER_OBJ;
  TOTAL_CONSTRS_COVER=_TOTAL_CONSTRS_COVER;

  srand (time(NULL)^(thread*nproc));

  createProblemStructures();

  return 1; 

}


int solver::initSolverAsCopy( CPXLPptr *_lp, int thread,double _TOTAL_CONSTRS_COVER,int _INITIAL_TILIM, double _TOTAL_CONSTRS_COVER_OBJ,int _INITIAL_TILIM_OBJ,double _DELTA_TOLERANCE, double _INIT_FIXING_COVER, int NTHREADS, int _nproc){
  
  CPLEXTHREADS=omp_get_num_procs()/(2*NTHREADS);
  NUM_THREADS=NTHREADS;
  TOTAL_CONSTRS_COVER=_TOTAL_CONSTRS_COVER;
  TOTAL_CONSTRS_COVER_OBJ=_TOTAL_CONSTRS_COVER;
  INITIAL_TILIM=_INITIAL_TILIM;
  DELTA_TOLERANCE=_DELTA_TOLERANCE;
  INIT_FIXING_COVER=_INIT_FIXING_COVER;
  nproc=_nproc;
  int status;

  env = CPXopenCPLEX (&status);
  lp = CPXcloneprob (env, *_lp, &status);
  originalLp= CPXcloneprob (env, lp, &status);
  
  tilimObj=_INITIAL_TILIM_OBJ;
  tilim=_INITIAL_TILIM;
  TOTAL_CONSTRS_COVER_OBJ=_TOTAL_CONSTRS_COVER_OBJ;
  TOTAL_CONSTRS_COVER=_TOTAL_CONSTRS_COVER;
  srand (time(NULL)^(thread*nproc));

  createProblemStructures();

  return 1; 
}

void solver::createProblemStructures(){

  int status;

  status = CPXsetintparam (env, CPX_PARAM_PREIND , 1); 
  status = CPXsetintparam (env, CPX_PARAM_SCRIND , PRINTOUTCPLEX);
  status = CPXsetintparam (env, CPX_PARAM_THREADS  , CPLEXTHREADS); 
  status = CPXsetdblparam (env, CPX_PARAM_TILIM  , 3600);
  status = CPXsetintparam(env, CPX_PARAM_MIPEMPHASIS,CPX_MIPEMPHASIS_HIDDENFEAS);
  status = CPXsetintparam(env, CPX_PARAM_PARALLELMODE,CPX_PARALLEL_OPPORTUNISTIC);

  numVars=CPXgetnumcols(env,lp);
  numConsts= CPXgetnumrows(env,lp);

  int trueLength=0;
  int tmp1;

  constBeginning = vector<int> (numConsts+1,0);

  CPXgetrows(env, lp, &tmp1, &constBeginning[0], 0, 0, 0, &trueLength, 0, numConsts-1);
  trueLength=-trueLength;
  constBeginning[numConsts]=trueLength;

  constIndices= vector<int> (trueLength);
  constCoefs = vector<double> (trueLength);
  constRHS=vector<double> (numConsts);
  constSenses = vector<char> (numConsts);
  MaxConstsSenses= vector<char> (numConsts);

  CPXgetrows(env, lp, &tmp1, &constBeginning[0], &constIndices[0], &constCoefs[0], trueLength, &trueLength, 0, numConsts-1);

  status=CPXgetrhs(env,lp,&constRHS[0],0,numConsts-1);
  status = CPXgetsense (env, lp, &constSenses[0],0,numConsts-1);

  MaxConstsSenses=constSenses;
  integerVariableIndices = new vector<int>(CPXgetnumbin (env, lp)+CPXgetnumint (env, lp));

  int ptr=0;
 
  varTypes= vector<char>(numVars);
  status = CPXgetctype (env, lp, &varTypes[0], 0, numVars-1);

  for (int i=0; i< numVars;i++){ 
    if (varTypes[i]=='B' || varTypes[i]=='I' ){
      (*integerVariableIndices)[ptr]=i;
      ptr++;
    }
  }

  //Obtaining the objective coefficients  
  variableObjectiveCoefficients = vector<double> (numVars);
  maxVarsObjectiveCoefficients = vector<double> (numConsts*2,1);
  
  status = CPXgetobj (env, lp, &variableObjectiveCoefficients[0], 0, numVars-1);

  //Initialization of the rest of the problem structures
  int ccnt= numConsts *2;
  int nzcnt=0;
  vector<char *> varNames(ccnt);
  for (unsigned int i=0; i< varNames.size();i+=2){
    std::string namep("MaxP"+std::to_string((long long int) i/2));
    std::string namen("MaxN"+std::to_string((long long int) i/2));
    char *cstrP = new char[namep.length() + 1];
    char *cstrN = new char[namen.length() + 1];
    strcpy(cstrP, namep.c_str());
    strcpy(cstrN, namen.c_str());
    varNames[i]= cstrP;
    varNames[i+1]= cstrN;
  }
  vector<double> obj(ccnt,1);
  vector<double> lb (ccnt,0);
  status = CPXaddcols (env, lp, ccnt, nzcnt, &obj[0], NULL, NULL, NULL, &lb[0], NULL, &varNames[0]);
  numberOfMaxConsts=numConsts;
  numberOfNonZeroCoefs=constIndices.size()+2*numConsts;
  MaxConstsRHS= vector<double>(numberOfMaxConsts,0);
  MaxConstsBeginning= vector<int> (numberOfMaxConsts+1,0);
  MaxConstsIndices=vector<int> (numberOfNonZeroCoefs,0);
  MaxConstsCoefs=vector<double> (numberOfNonZeroCoefs,1);
  MaxConstsNames=vector<char *> (numberOfMaxConsts);

  MaxConstsBeginning[0]=0;
  for (int i=0; i< numberOfMaxConsts; i++){
    int siz=constBeginning[i+1]-constBeginning[i];
    MaxConstsBeginning[i+1]=MaxConstsBeginning[i]+siz+2;
    std::string namec1("MaxConst"+std::to_string((long long int )i));
    char *cstrc1 = new char[namec1.length() + 1];
    strcpy(cstrc1, namec1.c_str());
    MaxConstsNames[i]= cstrc1;
    int offset=MaxConstsBeginning[i];
    for (int j=constBeginning[i]; j< constBeginning[i+1]; j++){
      MaxConstsIndices[offset]=constIndices[j];
      MaxConstsCoefs[offset]=constCoefs[j];
      offset++;
    }
    MaxConstsIndices[offset]=numVars+2*i;
    MaxConstsIndices[offset+1]=numVars+2*i+1;
    MaxConstsCoefs[offset]=1;
    MaxConstsCoefs[offset+1]=-1;
    MaxConstsRHS[i]=constRHS[i]; 

  }

  CPXdelrows(env,lp,0,numConsts-1);
  status = CPXaddrows (env, lp, 0, numberOfMaxConsts, numberOfNonZeroCoefs, &MaxConstsRHS[0], &MaxConstsSenses[0], &MaxConstsBeginning[0], &MaxConstsIndices[0], &MaxConstsCoefs[0], NULL, &MaxConstsNames[0]);

  savedVariableLowerBounds = vector<double>(numVars);
  savedVariableUpperBounds= vector<double>(numVars);

  status = CPXgetlb (env, lp, &savedVariableLowerBounds[0], 0, numVars-1);
  status = CPXgetub (env, lp, &savedVariableUpperBounds[0], 0, numVars-1);

  vector<int> indices1(numVars);
  vector<int> indices2(numConsts*2);
  for (unsigned int i=0; i< indices1.size(); i++)indices1[i]=i; 
  for (unsigned int i=0; i< indices2.size(); i++)indices2[i]=i+numVars;

  vector<double> zerovariableObjectiveCoefficients(numVars,0);
  status = CPXchgobj (env, lp, indices1.size(), &indices1[0], &zerovariableObjectiveCoefficients[0]);
  vector<int> maxVarsToFix;
 
  for (int i=0; i< numConsts; i++){
    if (constSenses[i]=='L'){
      maxVarsObjectiveCoefficients[i*2]= 0;
      maxVarsObjectiveCoefficients[i*2+1]= 1;
    }
    else if (constSenses[i]=='G'){
      maxVarsObjectiveCoefficients[i*2]= 1;
      maxVarsObjectiveCoefficients[i*2+1]= 0;
    }
    else{
      maxVarsObjectiveCoefficients[i*2]= 1;
      maxVarsObjectiveCoefficients[i*2+1]= 1;
    }
  }

  vector<double> zeroVals(maxVarsToFix.size(),0);

  status = CPXchgobj (env, lp, indices2.size(), &indices2[0], &maxVarsObjectiveCoefficients[0]);

  tilim=INITIAL_TILIM;
  tilimObj=INITIAL_TILIM;
  savedDeltaLowerBounds=vector<double>(numConsts*2);
  savedDeltaUpperBounds=vector<double>(numConsts*2);

  status = CPXgetlb (env, lp, &savedDeltaLowerBounds[0], numVars, CPXgetnumcols(env,lp)-1);
  status = CPXgetub (env, lp, &savedDeltaUpperBounds[0], numVars, CPXgetnumcols(env,lp)-1);

  //Preparing the objlp
  objLp= CPXcloneprob (env, lp, &status);

  //Change objective
  vector<double> zeromaxVarsObjectiveCoefficients(numConsts*2,0);

  status = CPXchgobj (env, objLp, indices1.size(), &indices1[0], &variableObjectiveCoefficients[0]);
  status = CPXchgobj (env, objLp, indices2.size(), &indices2[0], &zeromaxVarsObjectiveCoefficients[0]);

  //Also add delta cutoff
  vector<int> coefIndices;
  double cutOffRHS=CPX_INFBOUND;
  for (int i=0; i< numConsts; i++){
    if (maxVarsObjectiveCoefficients[i*2]==1){
      coefIndices.push_back(numVars+i*2);
    }
    if (maxVarsObjectiveCoefficients[i*2+1]==1){
      coefIndices.push_back(numVars+i*2+1);
    }
  }

  vector<double> coefficients(coefIndices.size(),1);

  int beg=0;

  char cutOffDir='L';

  CPXaddrows (env, objLp, 0, 1, coefficients.size(), &cutOffRHS, &cutOffDir, &beg, &coefIndices[0], &coefficients[0], NULL, NULL);

}

void solver::setStartingTimestamp(double time){
  startingTimestamp=time;
}

//accessor/setter methods
int solver::getNVars(){
  return CPXgetnumcols(env,lp);
}

void solver::changeObjTimeLim(double change){
  tilimObj+=change;
  if (tilimObj<0)tilimObj=TIME_CHANGE;
}

void solver::changeDeltaTimeLim(double change){
  tilim+=change;
  if (tilim<0)tilim=TIME_CHANGE;
}

void solver::changeObjCoverage(double change){
  TOTAL_CONSTRS_COVER_OBJ+=change;
  if (TOTAL_CONSTRS_COVER_OBJ<0)TOTAL_CONSTRS_COVER_OBJ=0;
}

void solver::changeDeltaCoverage(double change){
  TOTAL_CONSTRS_COVER+=change;
  if (TOTAL_CONSTRS_COVER<0)TOTAL_CONSTRS_COVER=0;
}

//solving methods
void solver::solveRelaxed(vector<double> &retvals, double &objVal, int &fracVals, int &iterationsPerformed){
  
  iterationsPerformed=0;
  int status=0;
  status = CPXsetintparam (env, CPX_PARAM_THREADS  , CPLEXTHREADS*NUM_THREADS); 

  //Creating a copy of the problem
  vector<int> indices1(numVars,0);
  vector<int> indices2(maxVarsObjectiveCoefficients.size());
  for (unsigned int i=0; i<indices1.size();i++)indices1[i]=i;  
  for (unsigned int i=0; i<indices2.size();i++)indices2[i]=i+numVars;

  CPXLPptr copy_lp = CPXcloneprob(env, lp, &status);

  vector<char> newTypes(indices1.size(),'C');
  status = CPXchgctype (env, copy_lp, numVars, &indices1[0], &newTypes[0]);

  //obtaining the variables sorted by bound range
  vector<pair <int, long long int> > arrayOfBounds((*integerVariableIndices).size());
  for (unsigned int i=0; i< (*integerVariableIndices).size(); i++){
    int varn=(*integerVariableIndices)[i];
    arrayOfBounds[i].first=varn;
    double lb = savedVariableLowerBounds[varn];
    double ub = savedVariableUpperBounds[varn];
    if (ub>10e6)ub=10e6;
    if (lb<-10e6)lb=-10e6;
    arrayOfBounds[i].second=ub-lb;
  } 
  std::random_shuffle ( arrayOfBounds.begin(), arrayOfBounds.end() );
  std::sort (arrayOfBounds.begin(), arrayOfBounds.end(), boundVectorSort);
  vector<double> output(numVars,-1);
  vector<double> lastSol;

  bool found=false;
  unsigned int fixedPtr=0;
  int chunkToBeFixed= (*integerVariableIndices).size()*INIT_FIXING_COVER;
  if (chunkToBeFixed<=0)chunkToBeFixed=1;
  int totalFixed=0;
  int its=0;
  while (!found){
    
  //Decide which vars to fix
    vector<int> varsToFix;
    vector<double> valsOfVarsToFix;
    int counter=0;
    while (fixedPtr<arrayOfBounds.size() &&counter<chunkToBeFixed){
      int var = arrayOfBounds[fixedPtr].first;
      if(output[var]==-1){
        double lb = savedVariableLowerBounds[var];
        double ub = savedVariableUpperBounds[var];
        if (ub>10e6)ub=10e6;
        if (lb<-10e6)lb=-10e6;
        double r=randomNumber(lb,ub,varTypes[var]);
        varsToFix.push_back(var);
        valsOfVarsToFix.push_back(r);
        counter++;
        output[var]=r;
      }

      fixedPtr++;
    }

    //Fix , relax everything else, solve
    if(varsToFix.size()>0){
      vector<char> fixDir (varsToFix.size(),'B');
      status = CPXchgbds (env, copy_lp, varsToFix.size(), &varsToFix[0], &fixDir[0], &valsOfVarsToFix[0]);
    }
    totalFixed+=varsToFix.size();
    iterationsPerformed++;
    status = CPXmipopt (env, copy_lp);

    vector<double> MIPvarArr(CPXgetnumcols(env,copy_lp),0);

    int mipstat;
    double MIPobjValue;

    status = CPXsolution (env, copy_lp, &mipstat, &MIPobjValue, &MIPvarArr[0], NULL, NULL, NULL);

    vector<int> newFixingsVars;
    vector<double> newFixingsVals;
    
    found=true;
    //Fix everything that became integral
    for (unsigned int i=0; i<(*integerVariableIndices).size(); i++){
      int varn = (*integerVariableIndices)[i];
      //Test if output is integral, then fix
      if (output[varn]==-1 && testInteger(MIPvarArr[varn])){
        newFixingsVars.push_back(varn);
        newFixingsVals.push_back(MIPvarArr[varn]);
        output[varn]=MIPvarArr[varn];
      }
      if (output[varn]==-1){
        found=false;
      }
    }
    totalFixed+=newFixingsVars.size();
    if(newFixingsVars.size()>0){
      vector<char> fixDir (newFixingsVars.size(),'B');
      status = CPXchgbds (env, copy_lp, newFixingsVars.size(), &newFixingsVars[0], &fixDir[0], &newFixingsVals[0]);
    } 
    lastSol=MIPvarArr;
    its++;
  }
  vector<int> violations;
  vector<double> slack;
  int numViolatedConstraints=collectInconsistencies(lastSol, violations, slack);
  for (int i=0; i< numVars+2*numConsts; i++){
    retvals[i]=lastSol[i];
  }
  fracVals=numViolatedConstraints;

  double objValue=0;
  for (int i=0; i< numVars; i++){
    objValue+=variableObjectiveCoefficients[i]*retvals[i];
  }
  objVal=objValue;

  status = CPXfreeprob (env, &copy_lp);
  status = CPXsetintparam (env, CPX_PARAM_THREADS  , CPLEXTHREADS); 
 
} 



int solver::performIteration(Solution &inSol, Solution &outSol, int iter,int nthread){

  CPXsetdblparam (env, CPX_PARAM_TILIM  ,tilim );

  vector<double> solValues;
  vector<int> violatedConstraints;
  vector<double> slacks;

  inSol.getValueVector(solValues);

  for (int i=0; i<numConsts*2; i++){
    if (solValues[numVars+i]<0)solValues[numVars+i]=0;
  }

  collectInconsistencies(solValues, violatedConstraints, slacks);

  int status=0;

  vector<int> indices1(numVars,0);
  vector<int> indices2(numConsts*2);
  for (unsigned int i=0; i<indices1.size();i++){
    indices1[i]=i;
  }
  for (unsigned int i=0; i<indices2.size();i++)indices2[i]=i+numVars;

  vector<char> ubdDirection (numVars,'U');

  status = CPXchgbds (env, lp, numVars, &indices1[0], &ubdDirection[0], &savedVariableUpperBounds[0]);

  vector<char> lbdDirection (numVars,'L');

  status = CPXchgbds (env, lp, numVars, &indices1[0], &lbdDirection[0], &savedVariableLowerBounds[0]);

  vector<char> ubdDirectionDelta (numConsts*2,'U');

  status = CPXchgbds (env, lp, numConsts*2, &indices2[0], &ubdDirectionDelta[0], &savedDeltaUpperBounds[0]);

  vector<char> lbdDirectionDelta (numConsts*2,'L');

  status = CPXchgbds (env, lp, numConsts*2, &indices2[0], &lbdDirectionDelta[0], &savedDeltaLowerBounds[0]);

  //Select starting point randomly and fix consecutively
  vector<bool> brokenVars(numVars,false);

  int totalBrokenVars=0;
  if(violatedConstraints.size()<VIOLATED_CONSTR_THRESHOLD){
    for (unsigned int i=0; i<violatedConstraints.size(); i++){
      for (int j=constBeginning[violatedConstraints[i]]; j<constBeginning[violatedConstraints[i]+1];j++){
        totalBrokenVars+=(brokenVars[constIndices[j]]==0);
        brokenVars[constIndices[j]]=1;
      }
    }
  }
  while (totalBrokenVars>VIOLATED_VARS_THRESHOLD){
    int randPos=rand()%numVars;
    if (brokenVars[randPos]){
      brokenVars[randPos]=0;
      totalBrokenVars--;
    }
  }

  vector<bool> isVarFixed(numVars,false);
  int randomStart = rand()%numVars;
  int limit = (*integerVariableIndices).size() *TOTAL_CONSTRS_COVER;
  int added=0;
  vector <int> fixedIndices;
  vector <double> fixedValues;

  for (int i=0;i<numVars && added<limit; i++){
    if( !brokenVars[randomStart] && (varTypes[randomStart]=='B' || varTypes[randomStart]=='I')){
      fixedIndices.push_back(randomStart);
      fixedValues.push_back(solValues[randomStart]);
      isVarFixed[randomStart]=true;
      added++;
    }
    randomStart++;
    if(randomStart>=numVars)randomStart=0;
  }

  if(fixedIndices.size()>0){
    vector<char> fixDir (fixedIndices.size(),'B');
    status = CPXchgbds (env, lp, fixedIndices.size(), &fixedIndices[0], &fixDir[0], &fixedValues[0]);
  }

  //add mipstart
  int mcnt=1;
  int beg=0;

  vector<int> mipstartIndex(solValues.size());

  for (unsigned int i=0; i<mipstartIndex.size();i++)mipstartIndex[i]=i; 

  status = CPXaddmipstarts (env, lp, mcnt, solValues.size(), &beg, &mipstartIndex[0], &solValues[0], CPX_MIPSTART_AUTO , NULL);

  if (PRINTOUT && nthread==0 && nproc==0) {   
    std::string nameMIP("MIPmodel"+std::to_string((long long int) iter)+".lp");
    status = CPXwriteprob (env, lp, nameMIP.c_str(), NULL);
  }

  status = CPXmipopt (env, lp);

  int numSolns = CPXgetsolnpoolnumsolns (env, lp);

  if (numSolns==0)return -1;

  vector<double> MIPvarArr(CPXgetnumcols(env,lp));

  int mipstat;

  double MIPobjValue;

  status = CPXsolution (env, lp, &mipstat, &MIPobjValue, &MIPvarArr[0], NULL, NULL, NULL);

  for (unsigned int i=0; i< MIPvarArr.size(); i++){
    if (abs(MIPvarArr[i])<EPSILON)MIPvarArr[i]=0;
  }
  for (unsigned int i=0; i< (*integerVariableIndices).size(); i++){
    int varn = (*integerVariableIndices)[i];
     MIPvarArr[varn]=roundToNearestInteger(MIPvarArr[varn]);
  }
  
  vector<int> indicesOfViolatedConstraints;

  vector<double> slack;

  double sumSlack=0;
  for (int i=0; i<numConsts;i++){
    if (constSenses[i]=='L'){
      sumSlack+=MIPvarArr[numVars+i*2+1];        
    }
    else if (constSenses[i]=='G'){
      sumSlack+=MIPvarArr[numVars+i*2];  
    }
    else{
      sumSlack+=MIPvarArr[numVars+i*2];  
      sumSlack+=MIPvarArr[numVars+i*2+1];  
    }
  }

  //Create solution and return
  double objValue=0;
  for (int i=0; i< numVars; i++){
    objValue+=variableObjectiveCoefficients[i]*MIPvarArr[i];
  }

  if (sumSlack<EPSILON)sumSlack=0;

  outSol.initSolution(MIPvarArr,sumSlack,objValue,iter,MIPobjValue,omp_get_wtime()-startingTimestamp);
  
  std::ostringstream os;
  os<<"REPORT:Value:"<<objValue<<":FRVals:"<<sumSlack<<":Iteration:"<<iter<<":Time:"<<omp_get_wtime()-startingTimestamp<<endl;
  std::cout<<os.str()<<std::flush;

  return 1;
}


int solver::performObjectiveImprovementIteration(Solution &inSol, Solution &outSol, int iter,int nthread){

  CPXsetdblparam (env, CPX_PARAM_TILIM  ,tilimObj );
  
  vector<double> solValues;
  vector<int> violatedConstraints;
  vector<double> slacks;

  inSol.getValueVector(solValues);

  collectInconsistencies(solValues, violatedConstraints, slacks);

  int status=0;

  vector<int> indices1(numVars,0);

  vector<int> indices2(numConsts*2);
  for (unsigned int i=0; i<indices1.size();i++){
    indices1[i]=i;
  }
  for (unsigned int i=0; i<indices2.size();i++)indices2[i]=i+numVars;

  vector<char> ubdDirection (numVars,'U');

  status = CPXchgbds (env, objLp, numVars, &indices1[0], &ubdDirection[0], &savedVariableUpperBounds[0]);

  vector<char> lbdDirection (numVars,'L');

  status = CPXchgbds (env, objLp, numVars, &indices1[0], &lbdDirection[0], &savedVariableLowerBounds[0]);

  vector<char> ubdDirectionDelta (numConsts*2,'U');

  vector<double> ubs (numConsts*2,CPX_INFBOUND);

  vector<double> deltaBds(numConsts*2);

  for (unsigned int i=0; i<deltaBds.size(); i++){
    if (solValues[numVars+i]<0)solValues[numVars+i]=0;
    deltaBds[i]=solValues[numVars+i]+DELTA_TOLERANCE;
  }

  status = CPXchgbds (env, objLp, numConsts*2, &indices2[0], &ubdDirectionDelta[0], &deltaBds[0]);

  vector<double> lbs (numConsts*2,0);

  vector<char> lbdDirectionDelta (numConsts*2,'L');

  status = CPXchgbds (env, objLp, numConsts*2, &indices2[0], &lbdDirectionDelta[0], &lbs[0]);

  int rowIndex=CPXgetnumrows(env,objLp)-1;

  double deltaRhs=inSol.getcutOffDeltaRHS();

  if (deltaRhs>EPSILON)deltaRhs+=DELTA_TOLERANCE;

  CPXchgrhs ( env, objLp, 1,&rowIndex,&deltaRhs);

  vector<bool> brokenVars(numVars,false);
  int totalBrokenVars=0;
  if(violatedConstraints.size()<VIOLATED_CONSTR_THRESHOLD){
    for (unsigned int i=0; i<violatedConstraints.size(); i++){
      for (int j=constBeginning[violatedConstraints[i]]; j<constBeginning[violatedConstraints[i]+1];j++){
        totalBrokenVars+=(brokenVars[constIndices[j]]==0);
        brokenVars[constIndices[j]]=1;
      }
    }
  }
  while (totalBrokenVars>VIOLATED_VARS_THRESHOLD){
    int randPos=rand()%numVars;
    if (brokenVars[randPos]){
      brokenVars[randPos]=0;
      totalBrokenVars--;
    }
  }

  int randomStart = rand()%numVars;
  int limit = (*integerVariableIndices).size() *TOTAL_CONSTRS_COVER_OBJ;
  int added=0;
  vector <int> fixedIndices;
  vector <double> fixedValues;
  for (int i=0;i<numVars && added<limit; i++){
    if( !brokenVars[randomStart] && (varTypes[randomStart]=='B' || varTypes[randomStart]=='I')){
      fixedIndices.push_back(randomStart);
      fixedValues.push_back( solValues[randomStart]);
      added++;
    }
    randomStart++;
    if(randomStart>=numVars)randomStart=0;
  }

  if(fixedIndices.size()>0){
    vector<char> fixDir (fixedIndices.size(),'B');
    status = CPXchgbds (env, objLp, fixedIndices.size(), &fixedIndices[0], &fixDir[0], &fixedValues[0]);
  } 

  //add mipstart
  int mcnt=1;
  int beg=0;

  vector<int> mipstartIndex(solValues.size());
  for (unsigned int i=0; i<mipstartIndex.size();i++)mipstartIndex[i]=i; 

  status = CPXaddmipstarts (env, objLp, mcnt, solValues.size(), &beg, &mipstartIndex[0], &solValues[0], CPX_MIPSTART_AUTO , NULL);
 
  if (PRINTOUT && nthread==0 && nproc==0) {   
    std::string nameMIP("OBJmodel"+std::to_string((long long int) iter)+".lp");
    status = CPXwriteprob (env, objLp, nameMIP.c_str(), NULL);
  }

  status = CPXmipopt (env, objLp);

  int numSolns = CPXgetsolnpoolnumsolns (env, objLp);
  if (numSolns==0)return -1;

  vector<double> MIPvarArr(CPXgetnumcols(env,objLp));

  int mipstat;

  double MIPobjValue;

  status = CPXsolution (env, objLp, &mipstat, &MIPobjValue, &MIPvarArr[0], NULL, NULL, NULL);
  
  for (unsigned int i=0; i< MIPvarArr.size(); i++){
    if (abs(MIPvarArr[i])<EPSILON)MIPvarArr[i]=0;
  }
  for (unsigned int i=0; i< (*integerVariableIndices).size(); i++){
    int varn = (*integerVariableIndices)[i];
    MIPvarArr[varn]=roundToNearestInteger(MIPvarArr[varn]);
  }

  vector<int> indicesOfViolatedConstraints;

  vector<double> slack;

  double sumSlack=0;
  for (int i=0; i<numConsts;i++){
    if (constSenses[i]=='L'){
      sumSlack+=MIPvarArr[numVars+i*2+1];        
    }
    else if (constSenses[i]=='G'){
      sumSlack+=MIPvarArr[numVars+i*2];  
    }
    else{
      sumSlack+=MIPvarArr[numVars+i*2];  
      sumSlack+=MIPvarArr[numVars+i*2+1];  
    }
  }

  //Create solution and return
  double objValue=0;
  for (int i=0; i< numVars; i++){
    objValue+=variableObjectiveCoefficients[i]*MIPvarArr[i];
  }

  if (sumSlack<EPSILON)sumSlack=0;

  outSol.initSolution(MIPvarArr,sumSlack,objValue,iter,inSol.getcutOffDeltaRHS(),omp_get_wtime()-startingTimestamp);

  std::ostringstream os;
  os<<"REPORT:Value:"<<objValue<<":FRVals:"<<sumSlack<<":Iteration:"<<iter<<":Time:"<<omp_get_wtime()-startingTimestamp<<endl;
  std::cout<<os.str()<<std::flush;

  return 1;

}    

int solver::compactSolutionsForDelta(vector<Solution> &DeltaSols, Solution &outSol, int iter){

  CPXsetintparam (env, CPX_PARAM_THREADS  , CPLEXTHREADS*NUM_THREADS); 

  int status = CPXsetdblparam (env, CPX_PARAM_TILIM  , tilim*2);

  vector<int> indices1(numVars,0);

  vector<int> indices2(numConsts*2);
  for (unsigned int i=0; i<indices1.size();i++) indices1[i]=i;

  for (unsigned int i=0; i<indices2.size();i++)indices2[i]=i+numVars;

  vector<char> ubdDirection (numVars,'U');

  status = CPXchgbds (env, lp, numVars, &indices1[0], &ubdDirection[0], &savedVariableUpperBounds[0]);

  vector<char> lbdDirection (numVars,'L');

  status = CPXchgbds (env, lp, numVars, &indices1[0], &lbdDirection[0], &savedVariableLowerBounds[0]);

  //iterate over vars, fix all that are equal

  int numCols = (numVars+2*numConsts);
  int numSols= DeltaSols.size();

  vector<int> mipstartIndex(numCols);

  for (unsigned int i=0; i<mipstartIndex.size();i++)mipstartIndex[i]=i; 

  int mcnt=1;
  int beg=0;

  vector<double> referenceSolution;
  DeltaSols[0].getValueVector(referenceSolution);

  status = CPXaddmipstarts (env, lp, mcnt, numCols, &beg, &mipstartIndex[0], &referenceSolution[0], CPX_MIPSTART_AUTO , NULL);
  
  vector<double> upperDeltaBounds(numConsts*2,0);
  for (int i=0; i< numConsts*2; i++)upperDeltaBounds[i]=referenceSolution[numVars+i];
  
  vector<bool> differentVars((*integerVariableIndices).size(),0);
  int bestPerformingSol=0;
  int bestPerfObj=DeltaSols[0].getcutOffDeltaRHS();

  for (int p=1; p<numSols; p++){
    if (bestPerfObj>DeltaSols[p].getcutOffDeltaRHS()){
      bestPerformingSol=p;
      bestPerfObj=DeltaSols[p].getcutOffDeltaRHS();
    }
    vector<double> threadSolution;
    DeltaSols[p].getValueVector(threadSolution);
    status = CPXaddmipstarts (env, lp, mcnt, numCols, &beg, &mipstartIndex[0], &threadSolution[0], CPX_MIPSTART_AUTO , NULL);
    for (unsigned int i=0; i<(*integerVariableIndices).size(); i++){
      int varn = (*integerVariableIndices)[i];
      if(threadSolution[varn]!=referenceSolution[varn])differentVars[i]=true;
    } 
    for (int i=0; i< numConsts*2;i++){
      if (threadSolution[numVars+i]>upperDeltaBounds[i])upperDeltaBounds[i]=threadSolution[numVars+i]+DELTA_TOLERANCE;
    }
  }

  vector<char> ubdDirectionDelta (numConsts*2,'U');
  status = CPXchgbds (env, lp, numConsts*2, &indices2[0], &ubdDirectionDelta[0], &upperDeltaBounds[0]);

  vector<int> fixVarIndices;
  vector<double> fixVarValues;
  int numFixings=0;
  for (unsigned int i=0; i<differentVars.size(); i++){
    if (!differentVars[i]){
    numFixings++;
    }
  }
  DeltaSols[bestPerformingSol].getValueVector(referenceSolution);
  int limit = (*integerVariableIndices).size() *TOTAL_CONSTRS_COVER;
  while (numFixings<limit){
    int randomInt=rand()%(*integerVariableIndices).size();
    if (differentVars[randomInt]){
      numFixings++;
      differentVars[randomInt]=false;
    }
  }
  while (numFixings>limit){
    int randomInt=rand()%(*integerVariableIndices).size();
      if (!differentVars[randomInt]){
      numFixings--;
      differentVars[randomInt]=true;
    }
  }
  for (unsigned int i=0; i<differentVars.size(); i++){
    if (!differentVars[i]){
      fixVarIndices.push_back((*integerVariableIndices)[i]);
      fixVarValues.push_back(referenceSolution[(*integerVariableIndices)[i]]);
    }
  }

  if(fixVarIndices.size()>0){
    vector<char> fixDir (fixVarIndices.size(),'B');
    status = CPXchgbds (env, lp, fixVarIndices.size(), &fixVarIndices[0], &fixDir[0], &fixVarValues[0]);
  } 

  status = CPXmipopt (env, lp);

  int numSolns = CPXgetsolnpoolnumsolns (env, lp);
  if (numSolns==0)return -1;

  vector<double> MIPvarArr(CPXgetnumcols(env,lp));

  int mipstat;

  double MIPobjValue;

  status = CPXsolution (env, lp, &mipstat, &MIPobjValue, &MIPvarArr[0], NULL, NULL, NULL);

  for (unsigned int i=0; i< MIPvarArr.size(); i++){
    if (abs(MIPvarArr[i])<EPSILON)MIPvarArr[i]=0;
  }
  for (unsigned int i=0; i< (*integerVariableIndices).size(); i++){
    int varn = (*integerVariableIndices)[i];
     MIPvarArr[varn]=roundToNearestInteger(MIPvarArr[varn]);
  }

  vector<int> indicesOfViolatedConstraints;

  vector<double>  retVector(CPXgetnumcols(env,lp));
  for (unsigned int i=0; i< retVector.size(); i++)retVector[i]=MIPvarArr[i];
  vector<double> slack;
  double sumSlack=0;
  for (int i=0; i<numConsts;i++){
    if (constSenses[i]=='L'){
      sumSlack+=MIPvarArr[numVars+i*2+1];        
    }
    else if (constSenses[i]=='G'){
      sumSlack+=MIPvarArr[numVars+i*2];  
    }
    else{
      sumSlack+=MIPvarArr[numVars+i*2];  
      sumSlack+=MIPvarArr[numVars+i*2+1];  
    }
  }

  //Create solution and return
  double objValue=0;
  for (int i=0; i< numVars; i++){
    objValue+=variableObjectiveCoefficients[i]*MIPvarArr[i];
  }
  
  if (sumSlack<EPSILON)sumSlack=0;

  outSol.initSolution(retVector,sumSlack,objValue,iter,MIPobjValue,omp_get_wtime()-startingTimestamp);
  
  std::ostringstream os;
  os<<"REPORT:Value:"<<objValue<<":FRVals:"<<sumSlack<<":Iteration:"<<iter<<":Time:"<<omp_get_wtime()-startingTimestamp<<endl;
  std::cout<<os.str()<<std::flush;
 
  status = CPXsetintparam (env, CPX_PARAM_THREADS  , CPLEXTHREADS); 

  return 1;
}


int solver::compactSolutionsForObj(vector<Solution> &Sols, Solution &outSol, int iter){

  CPXsetintparam (env, CPX_PARAM_THREADS  , CPLEXTHREADS*NUM_THREADS); 

  int status = CPXsetdblparam (env, CPX_PARAM_TILIM  , tilimObj*2);

  vector<int> indices1(numVars,0);
  vector<int> indices2(numConsts*2);
  for (unsigned int i=0; i<indices1.size();i++)indices1[i]=i;
  for (unsigned int i=0; i<indices2.size();i++)indices2[i]=i+numVars;

  vector<char> ubdDirection (numVars,'U');
  status = CPXchgbds (env, objLp, numVars, &indices1[0], &ubdDirection[0], &savedVariableUpperBounds[0]);

  vector<char> lbdDirection (numVars,'L');
  status = CPXchgbds (env, objLp, numVars, &indices1[0], &lbdDirection[0], &savedVariableLowerBounds[0]);

  vector<char> ubdDirectionDelta (numConsts*2,'U');

  vector<double> ubs (numConsts*2,CPX_INFBOUND);

  status = CPXchgbds (env, objLp, numConsts*2, &indices2[0], &ubdDirectionDelta[0], &ubs[0]);

  vector<double> lbs (numConsts*2,0);

  vector<char> lbdDirectionDelta (numConsts*2,'L');

  status = CPXchgbds (env, objLp, numConsts*2, &indices2[0], &lbdDirectionDelta[0], &lbs[0]);

  //Restore objective 
  int numCols = (numVars+2*numConsts);
  int numSols= Sols.size();

  vector<int> mipstartIndex(numCols);
  for (unsigned int i=0; i<mipstartIndex.size();i++)mipstartIndex[i]=i; 

  int mcnt=1;
  int beg=0;

  vector<double> referenceSolution;
  Sols[0].getValueVector(referenceSolution);
  status = CPXaddmipstarts (env, objLp, mcnt, numCols, &beg, &mipstartIndex[0], &referenceSolution[0], CPX_MIPSTART_AUTO , NULL);

  vector<bool> differentVars((*integerVariableIndices).size(),0);
  vector<double> deltaBds(numConsts*2);
  double minDeltaRHS=Sols[0].getcutOffDeltaRHS();
  for (unsigned int i=0; i<deltaBds.size(); i++)deltaBds[i]=referenceSolution[numVars+i]+DELTA_TOLERANCE;
  int bestPerformingSol=0;
  int bestPerfObj=Sols[0].getValue();
  for (int p=1; p<numSols; p++){
    if (bestPerfObj>Sols[p].getcutOffDeltaRHS()){
      bestPerformingSol=p;
      bestPerfObj=Sols[p].getcutOffDeltaRHS();
    }
    if (Sols[p].getcutOffDeltaRHS()>minDeltaRHS)minDeltaRHS=Sols[p].getcutOffDeltaRHS();
    vector<double> threadSolution;
    Sols[p].getValueVector(threadSolution);
    status = CPXaddmipstarts (env, objLp, mcnt, numCols, &beg, &mipstartIndex[0], &threadSolution[0], CPX_MIPSTART_AUTO , NULL);
    for (unsigned int i=0; i<(*integerVariableIndices).size(); i++){
      int varn = (*integerVariableIndices)[i];
      if(threadSolution[varn]!=referenceSolution[varn])differentVars[i]=true;
    }
    for (unsigned int i=0; i<deltaBds.size(); i++)deltaBds[i]=max(deltaBds[i], threadSolution[numVars+i]+1e-6);
  }
  if (iter>0) status = CPXchgbds (env, objLp, numConsts*2, &indices2[0], &ubdDirectionDelta[0], &deltaBds[0]);

  vector<int> fixVarIndices;
  vector<double> fixVarValues;
  int numFixings=0;
  for (unsigned int i=0; i<differentVars.size(); i++){
    if (!differentVars[i]){
      numFixings++;
    }
  }
  Sols[bestPerformingSol].getValueVector(referenceSolution);
  int limit = (*integerVariableIndices).size() *TOTAL_CONSTRS_COVER;
  while (numFixings<limit){
    int randomInt=rand()%(*integerVariableIndices).size();
    if (differentVars[randomInt]){
      numFixings++;
      differentVars[randomInt]=false;
    }
  }
  while (numFixings>limit){
    int randomInt=rand()%(*integerVariableIndices).size();
    if (!differentVars[randomInt]){
      numFixings--;
      differentVars[randomInt]=true;
    }
  }

  for (unsigned int i=0; i<differentVars.size(); i++){
    if (!differentVars[i]){
      fixVarIndices.push_back((*integerVariableIndices)[i]);
      fixVarValues.push_back(referenceSolution[(*integerVariableIndices)[i]]);
    }
  }

  if(fixVarIndices.size()>0){
    vector<char> fixDir (fixVarIndices.size(),'B');
    status = CPXchgbds (env, objLp, fixVarIndices.size(), &fixVarIndices[0], &fixDir[0], &fixVarValues[0]);
  } 

  status = CPXmipopt (env, objLp);

  int numSolns = CPXgetsolnpoolnumsolns (env, objLp);
  if (numSolns==0)return -1;

  vector<double> MIPvarArr(CPXgetnumcols(env,objLp));

  int mipstat;

  double MIPobjValue;

  status = CPXsolution (env, objLp, &mipstat, &MIPobjValue, &MIPvarArr[0], NULL, NULL, NULL);

  for (unsigned int i=0; i< MIPvarArr.size(); i++){
    if (abs(MIPvarArr[i])<EPSILON)MIPvarArr[i]=0;
  }
  for (unsigned int i=0; i< (*integerVariableIndices).size(); i++){
    int varn = (*integerVariableIndices)[i];
     MIPvarArr[varn]=roundToNearestInteger(MIPvarArr[varn]);
  }

  vector<int> indicesOfViolatedConstraints;

  vector<double>  retVector(CPXgetnumcols(env,objLp));
  for (unsigned int i=0; i< retVector.size(); i++)retVector[i]=MIPvarArr[i];
  vector<double> slack;

  double sumSlack=0;
  for (int i=0; i<numConsts;i++){
    if (constSenses[i]=='L'){
      sumSlack+=MIPvarArr[numVars+i*2+1];        
    }
    else if (constSenses[i]=='G'){
      sumSlack+=MIPvarArr[numVars+i*2];  
    }
    else{
      sumSlack+=MIPvarArr[numVars+i*2];  
      sumSlack+=MIPvarArr[numVars+i*2+1];  
    }
  }

  //Create solution and return
  double objValue=0;
  for (int i=0; i< numVars; i++){
   objValue+=variableObjectiveCoefficients[i]*MIPvarArr[i];
  }

  if (sumSlack<EPSILON)sumSlack=0;

  outSol.initSolution(retVector,sumSlack,objValue,iter, minDeltaRHS,omp_get_wtime()-startingTimestamp);
  
  std::ostringstream os;
  os<<"REPORT:Value:"<<objValue<<":FRVals:"<<sumSlack<<":Iteration:"<<iter<<":Time:"<<omp_get_wtime()-startingTimestamp<<endl;
  std::cout<<os.str()<<std::flush;

  status = CPXsetintparam (env, CPX_PARAM_THREADS  , CPLEXTHREADS); 

  return 1;
}


//Auxiliary functions
int solver::collectInconsistencies(vector<double> &MIPvarArr, vector<int> &inconsistentIndices, vector<double> &slacks){
  int numViolatedConstraints=0;
  //Detect the number of violated constraints
  for (int i=0; i< numConsts;i++){
    double expression=0;
    for (int j=constBeginning[i]; j< constBeginning[i+1];j++){
      expression+=constCoefs[j]*MIPvarArr[constIndices[j]];
    }
    if(constSenses[i]=='L'){
      if (expression>constRHS[i]+EPSILON){
        slacks.push_back(expression-constRHS[i]);
        numViolatedConstraints++;
        inconsistentIndices.push_back(i);
      }
    }
    else if(constSenses[i]=='G'){
      if (expression+EPSILON<constRHS[i]){
        slacks.push_back(constRHS[i]-expression);
        numViolatedConstraints++;
        inconsistentIndices.push_back(i);
      }
    }
    else if (constSenses[i]=='E'){
      double diff =expression-constRHS[i];
      if (diff<0)diff=-diff;
      if (diff>EPSILON ){
        slacks.push_back(diff);
        numViolatedConstraints++;
        inconsistentIndices.push_back(i);
      }
    }
    else{
      cout<<"FATAL ERROR!"<<endl;
    }
  }
  return numViolatedConstraints;
}
