// ----------------------------------------------------------------------------
/**
   File: mainMPI.cpp

   Description: Main file of the heuristic. It contains the main execution loop,
                orquestrates the parallel search, as well as the recombination.
   Created by Lluis-Miquel Munguia Conejero (lluis.munguia@gatech.edu)
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/ 
// ----------------------------------------------------------------------------

#include <fstream>
#include "solver.h"
#include <hwloc.h>
using namespace std;    

#define MAX_STALE_ITERS 20
#define MAX_ITERS 3000

typedef struct {
  double  value;
  int    proc;
} double_int;

int main (int argc, char *argv[])
{

  int myid, numprocs;

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD,&myid); 

  double delta_cover=-1;
  int delta_time=-1;
  double obj_cover=-1;
  int obj_time=-1;
  double delta_tolerance=-1;
  double init_fixing_cover=-1;
  int n_threads=-1;
  int tilim=-1;
  string fileName="";
  string logFile="";
  string finalSolFile="";
  int c;

  while ((c = getopt (argc, argv, "f:c:t:d:i:o:L:O:s:h")) != -1)
    switch (c)
      {
      case 'f':
        fileName = optarg;
      break;
      case 'c':
        delta_cover = atof (optarg);
        obj_cover = atof (optarg);
      break;
      case 't':
        delta_time = atoi(optarg);
        obj_time = atoi(optarg);
      break;
      case 'd':
        delta_tolerance = atof (optarg);
      break;
      case 'i':
        init_fixing_cover = atof (optarg);
      break;
      case 'o':
        n_threads = atoi (optarg);
      break;
      case 'L':
        tilim = atoi (optarg);
      break;
      case 'O':
        logFile = optarg;
      break;
      case 's':
        finalSolFile = optarg;
      break;
      case 'h':
        if (myid==0){
          fprintf (stderr, "Usage: -f [Problem file] -c [Variable cover percentage] -t [LNS time limit] -d [Numerical tolerance] -i [Initial fixing cover] -o [Number of OpenMP threads] -L [Heuristic time limit] -O [Log output filename] -s [Final solution filename] \n");
        }
        MPI_Finalize();
        return 1;
      break;

      case '?':
        fprintf (stderr, "Unknown option `-%c'.\n", optopt);
      return 1;
      default:
        MPI_Finalize();
        break;
      }

    if (fileName == "")
    {
      printf ("ERROR: a problem file must be selected.\n");
      MPI_Finalize();
      return 1;
    }

  if (tilim==-1) tilim=3600;
    
  if (n_threads==-1){
    // Try to get the number of CPU cores from topology
    hwloc_topology_t topology;
    hwloc_topology_init(&topology);
    hwloc_topology_load(topology);
    int depth = hwloc_get_type_depth(topology, HWLOC_OBJ_CORE);

    if(depth == HWLOC_TYPE_DEPTH_UNKNOWN){
      printf("ERROR: NUMBER OF THREADS UNKNOWN: use option -o to specify them.\n");
      return 1;
    }
    else {
      n_threads=hwloc_get_nbobjs_by_depth(topology, depth);
    }
    // Destroy topology object and return
    hwloc_topology_destroy(topology);
  }

  cpu_set_t  mask;
  CPU_ZERO(&mask);
  for (int th=0; th < n_threads; th++){
    CPU_SET(th, &mask);
  }
  
  omp_set_num_threads(n_threads);
  char processor_name[MPI_MAX_PROCESSOR_NAME];

  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);
 

  //Statistics variables
  int deltaIterations=0;
  double deltaTotalTime=0;
  double deltaCombTotalTime=0;
  int objIterations=0;
  double objTotalTime=0;
  double objCombTotalTime=0;
  bool zeroSolFound=0;
  double timeStamp=omp_get_wtime();
  double idleTimeAccumulation=0;
  double idleTimeAccumulationComb=0;
  int itersWithoutImprov=0;

  //Solver creation
  vector<solver*> solv(n_threads);
  solv[0] = new solver();
  int success=solv[0]->initSolver(fileName,0,delta_cover,delta_time,obj_cover, obj_time,delta_tolerance,init_fixing_cover,n_threads,myid);
  if (success==0){
    if (myid==0) printf ("ERROR while reading the problem file.\n");
    MPI_Finalize();
    return 1;
  }
  if (myid==0){
    cout<<"--------------INITIAL HEURISTIC SETTINGS--------------"<<endl;
    cout<<"Initial Variable cover percentage:"<<delta_cover<<endl;
    cout<<"Initial LNS time:"<<delta_time<<endl;
    cout<<"Initial Numerical tolerance:"<<delta_tolerance<<endl;
    cout<<"Initial Variable fixing cover for Initialization:"<<init_fixing_cover<<endl;
    cout<<"Number of OpenMP Threads:"<<n_threads<<endl;
    cout<<"------------------------------------------------------"<<endl;
  }
  CPXLPptr *_lp= solv[0]->getOriginalProbLP();

  #pragma omp parallel for
  for (int i=1; i< n_threads;i++){
    solv[i] = new solver();
    solv[i]->initSolverAsCopy(_lp,i,delta_cover,delta_time,obj_cover, obj_time,delta_tolerance,init_fixing_cover,n_threads,myid);  
  }
  for (int i=0; i< n_threads;i++) solv[i]->setStartingTimestamp(timeStamp);

  //Generating first partial solution and adding it to the stack  
  vector<double> values(solv[0]->getNVars(),0);
  int fracVals=0;
  double objValue=0;
  int relaxationIterations=0;
  double relaxTimStart=omp_get_wtime();
  solv[0]->solveRelaxed(values, objValue, fracVals,relaxationIterations);
  double relaxTimEnd=omp_get_wtime();

  Solution sol;
  sol.initSolution(values,fracVals,objValue,0,0,omp_get_wtime()-timeStamp);

  //Initializing auxiliar solution buffers
  int solSize=sol.getSerialSize();
  vector<double> allIntermediateSolutions(numprocs*solSize);
  Solution* incumbentSolution = new Solution();

  if (sol.getFracVals()<EPSILON){
    zeroSolFound=true; 
  }
  //Main heuristic loop
  for (int iter=0; iter<MAX_ITERS && itersWithoutImprov<MAX_STALE_ITERS && omp_get_wtime()-timeStamp<tilim ; iter ++){
    int success=-1;
    
    if (!zeroSolFound) {    
      double deltaStepStartTime=omp_get_wtime();
      deltaIterations++;
      vector<Solution> DeltaSols(n_threads);
      double originalValue= sol.getFracVals();
      double partialidleTime=0;
    
      //Parallel FMIP step
      #pragma omp parallel reduction(+:partialidleTime)
      {
    
        int th=omp_get_thread_num();
        success=solv[th]->performIteration(sol, DeltaSols[th], iter,th);
        if (success==-1)DeltaSols[th]=sol;
        double timeCheckpoint=omp_get_wtime();
        #pragma omp barrier
        partialidleTime=omp_get_wtime()-timeCheckpoint;
      }
      idleTimeAccumulation+=partialidleTime;
      double deltaStepCombStartTime=omp_get_wtime();
      Solution intermediateSolution;
      if (n_threads>1){ 
        //Shared memory solution recombination        
        success=solv[0]->compactSolutionsForDelta(DeltaSols, intermediateSolution, iter);         
        if (success==-1)intermediateSolution=DeltaSols[0];
        double preSecondRecCheckpoint=omp_get_wtime();
        MPI_Barrier(MPI_COMM_WORLD);
         
        idleTimeAccumulationComb+=(omp_get_wtime()-preSecondRecCheckpoint);
      }
      else{
        intermediateSolution=DeltaSols[0];
      }

      if(numprocs>1){
        //Distributed memory solution recombination
        vector<double> localSolution(solSize);
        intermediateSolution.serializeSolution(localSolution);
        MPI_Allgather (&localSolution[0], solSize, MPI_DOUBLE, &allIntermediateSolutions[0], solSize, MPI_DOUBLE, MPI_COMM_WORLD);
       
        vector<Solution> secondRecombination(numprocs);
        for (unsigned int i=0; i< secondRecombination.size(); i++){
          secondRecombination[i].initSolution(&allIntermediateSolutions[i*solSize]);
        }
        success=solv[0]->compactSolutionsForDelta(secondRecombination, sol, iter);
        if (success==-1)sol=secondRecombination[0];
        //Decide who has the best solution, exchange it.

        double_int localres;
        double_int globalres;
        localres.value=sol.getcutOffDeltaRHS();
        localres.proc=myid;

        double postSecondRecCheckpoint=omp_get_wtime();
        MPI_Barrier(MPI_COMM_WORLD);

        idleTimeAccumulationComb+=(omp_get_wtime()-postSecondRecCheckpoint);

        MPI_Allreduce(&localres, &globalres, 1, MPI_DOUBLE_INT, MPI_MINLOC, MPI_COMM_WORLD);

        if (globalres.proc==myid){
          sol.serializeSolution(localSolution);
          MPI_Bcast( &localSolution[0],solSize,MPI_DOUBLE, globalres.proc,MPI_COMM_WORLD);
        }
        else{
          MPI_Bcast( &localSolution[0],solSize,MPI_DOUBLE, globalres.proc,MPI_COMM_WORLD);
          sol.initSolution(&localSolution[0]);
        }
        
      }
      else{
        sol=intermediateSolution;
      }

      deltaCombTotalTime+=(omp_get_wtime()-deltaStepCombStartTime);
      deltaTotalTime+=(omp_get_wtime()-deltaStepStartTime);
      double improvementValue= sol.getFracVals();
      if (improvementValue>=originalValue){
        for (int i=0; i< n_threads; i++){
          solv[i]->changeDeltaTimeLim(TIME_CHANGE);
          solv[i]->changeDeltaCoverage(COVER_CHANGE);
        }
      }

      if (myid==0 && logFile!="") { 
        ofstream file;
        file.open (logFile, ios::out | ios::app | ios::binary);
        file << "REPORT:Value:"<<sol.getValue()<<":FRVals:"<<sol.getFracVals()<<":Iteration:"<<iter<<":Time:"<<sol.getDiscoveryTime()<<"\n";
        file.close();
      }

    }
    
    (*incumbentSolution)=sol;

    //beginning of the OMIP iteration
    objIterations++;
    double objStepStartTime=omp_get_wtime();
    double originalObjValue= sol.getValue();

    vector<Solution> objSols(n_threads);
    double partialidleTime=0;
    //Parallel OMIP step
    #pragma omp parallel reduction(+:partialidleTime)
    {
      int th=omp_get_thread_num();
      success=solv[th]->performObjectiveImprovementIteration(sol, objSols[th], iter,th);
      if (success==-1){ 
        objSols[th]=sol;
      }
      double timeCheckpoint=omp_get_wtime();
      #pragma omp barrier
      partialidleTime=omp_get_wtime()-timeCheckpoint;
 
    }
    idleTimeAccumulation+=partialidleTime;
    double objStepRecStartTime=omp_get_wtime();
    Solution intermediateSolution;
    if (n_threads>1){ 
      //Shared memory solution recombination
      success=solv[0]->compactSolutionsForObj(objSols, intermediateSolution, iter);
      if (success==-1)intermediateSolution=objSols[0];
      double preSecondRecCheckpoint=omp_get_wtime();
      MPI_Barrier(MPI_COMM_WORLD);   
      idleTimeAccumulationComb+=(omp_get_wtime()-preSecondRecCheckpoint);
    }
    else{
      intermediateSolution=objSols[0];
    }

    if(numprocs>1){
      //Distributed memory solution recombination
      vector<double> localSolution2(solSize);
      intermediateSolution.serializeSolution(localSolution2);
      MPI_Allgather (&localSolution2[0], solSize, MPI_DOUBLE, &allIntermediateSolutions[0], solSize, MPI_DOUBLE, MPI_COMM_WORLD);
    
      vector<Solution> secondRecombination2(numprocs);
      for (unsigned int i=0; i< secondRecombination2.size(); i++){
        secondRecombination2[i].initSolution(&allIntermediateSolutions[i*solSize]);
      }
      success=solv[0]->compactSolutionsForObj(secondRecombination2, sol, iter);
      if (success==-1)sol=secondRecombination2[0];
      double_int localres;
      double_int globalres;
      localres.value=sol.getValue();
      localres.proc=myid;
       
      double postSecondRecCheckpoint=omp_get_wtime();
      MPI_Barrier(MPI_COMM_WORLD);
      idleTimeAccumulationComb+=(omp_get_wtime()-postSecondRecCheckpoint);

      MPI_Allreduce(&localres, &globalres, 1, MPI_DOUBLE_INT, MPI_MINLOC, MPI_COMM_WORLD);

      //Decide who has the best solution, exchange it.

      if (globalres.proc==myid){
        sol.serializeSolution(localSolution2);
        MPI_Bcast( &localSolution2[0],solSize,MPI_DOUBLE, globalres.proc,MPI_COMM_WORLD);
      } 
      else{
        MPI_Bcast( &localSolution2[0],solSize,MPI_DOUBLE, globalres.proc,MPI_COMM_WORLD);
        sol.initSolution(&localSolution2[0]);
      }
    }
    else{
      sol=intermediateSolution;
    }
    objTotalTime+=omp_get_wtime()-objStepStartTime;
    objCombTotalTime+=omp_get_wtime()-objStepRecStartTime;
    double finalObjValue= sol.getValue();
    if (originalObjValue==finalObjValue){
      itersWithoutImprov++;
      for (int i=0; i< n_threads; i++){
        solv[i]->changeObjTimeLim(TIME_CHANGE);
        solv[i]->changeObjCoverage(COVER_CHANGE);
      }
    }
    else{
      itersWithoutImprov=0;
    }

    (*incumbentSolution)=sol;

    if ((*incumbentSolution).getFracVals()<EPSILON){
      zeroSolFound=true;
    }

    if (myid==0 && logFile!="") { 
        ofstream file;
        file.open (logFile, ios::out | ios::app | ios::binary);
        file << "REPORT:Value:"<<(*incumbentSolution).getValue()<<":FRVals:"<<(*incumbentSolution).getFracVals()<<":Iteration:"<<iter<<":Time:"<<(*incumbentSolution).getDiscoveryTime()<<"\n";
        file.close();
    }

  }

  if (myid==0 && finalSolFile!=""){
    ofstream file;
    file.open (finalSolFile, ios::out  | ios::binary);
    int solSize=sol.getSerialSize();
    file<<solSize<<endl;
    file<<sol.getValue()<<endl;
    file<<sol.getcutOffDeltaRHS()<<endl;
    vector<double> serializedSolution(solSize);
    (*incumbentSolution).serializeSolution(serializedSolution);
    file <<serializedSolution[0];
    for(unsigned int i=1; i<serializedSolution.size();i++){
      file<<" "<<serializedSolution[i];
    }
    file<<endl;
    file.close();
  }

  double totalIdleTime=0;
  double totalIdleTimeComb=0;
  MPI_Allreduce(&idleTimeAccumulation,&totalIdleTime,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);
  MPI_Allreduce(&idleTimeAccumulationComb ,&totalIdleTimeComb  ,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD);

  if (myid==0){
    cout<<"--------------Heuristic Finished--------------"<<endl;
    cout<<endl;
    cout<<"++++++++++++++Initialization++++++++++++++"<<endl;
    cout<<"Iterations:"<<relaxationIterations<<":Time:"<<relaxTimEnd-relaxTimStart<<":Iterations:"<<relaxationIterations<<":FracVals:"<<fracVals<<endl;
    cout<<"++++++++++++++++++++++++++++++++++++++++++"<<endl;
    cout<<"++++++++++++++Delta MIP++++++++++++++"<<endl;
    cout<<"dIterations:"<<deltaIterations<<":Time:"<<deltaTotalTime<<":TimeInRecombStep:"<<deltaCombTotalTime<<endl;
    cout<<"++++++++++++++++++++++++++++++++++++++++++"<<endl;
    cout<<"++++++++++++++Obj MIP++++++++++++++"<<endl;
    cout<<"oIterations:"<<objIterations<<":Time:"<<objTotalTime<<":TimeInRecombStep:"<<objCombTotalTime<<endl;
    cout<<"++++++++++++++++++++++++++++++++++++++++++"<<endl;
    cout<<"++++++++Synchronization statistics++++++++"<<endl;
    cout<<"TotalTSync:"<<totalIdleTime+totalIdleTimeComb <<":Average TSync Time ("<<n_threads*numprocs<<"):"<<(totalIdleTime+totalIdleTimeComb)/(n_threads*numprocs)<<":ParallelLNSTSync:"<<totalIdleTime<<":CombTSync:"<<totalIdleTimeComb<<endl;
    cout<<"++++++++++++++++++++++++++++++++++++++++++"<<endl;
    
  }

  for (int i=0; i< n_threads;i++) delete solv[i];
  delete incumbentSolution;

  MPI_Finalize();
}