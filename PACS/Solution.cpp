// ----------------------------------------------------------------------------
/**
   File: Solution.cpp

   Description: Container class for candidate integer solutions. 
   Created by Lluis-Miquel Munguia Conejero (lluis.munguia@gatech.edu)
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/ 
// ----------------------------------------------------------------------------
#include "Solution.h"

Solution::Solution ()
{
 
}

Solution::~Solution ()
{

}


int Solution::initSolution(double *serializedSolution){

	values=vector<double> (serializedSolution[0]);
	fracVals=(int)serializedSolution[1];
	solVal=serializedSolution[2];
	cutOffDeltaRHS=serializedSolution[3];
	discoveryTime=serializedSolution[4];
	for (unsigned int i=0; i< values.size();i++)values[i]=serializedSolution[i+5];
	return 1;
}

int Solution::initSolution(vector<double> &_values, double _fracVals, double _solVal, int _iteration, double _cutOffDeltaRHS, double _discoveryTime){

	values=vector<double> (_values);
	fracVals=_fracVals;
	solVal=_solVal;
	iteration=_iteration;
    cutOffDeltaRHS=_cutOffDeltaRHS;
    discoveryTime=_discoveryTime;
	return 1;
}

double Solution::getValue() const{
	return solVal;
}

double Solution::getFracVals() const{
	return fracVals;
}

double Solution::getcutOffDeltaRHS() {
	return cutOffDeltaRHS;
}


void Solution::getValueVector(vector<double> &_values){
	_values=values;
}


void Solution::serializeSolution(vector<double> &serializedSolution){
	serializedSolution[0]=values.size();
	serializedSolution[1]=fracVals;
	serializedSolution[2]=solVal;
	serializedSolution[3]=cutOffDeltaRHS;
	serializedSolution[4]=discoveryTime;
	for (unsigned int i=0; i< values.size();i++)serializedSolution[i+5]=values[i];

}
int Solution::getSerialSize(){
	return values.size()+5;
}

double Solution::getDiscoveryTime(){
	return discoveryTime;
}
