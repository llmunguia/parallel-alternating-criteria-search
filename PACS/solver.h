// ----------------------------------------------------------------------------
/**
   File: solver.h

   Description: Class containing the main solver of the heuristic. The solver is
                responsible for maintaining updated representations of all the 
                optimization models, as well as finding improvements in feasibility 
                and optimality.
   Created by Lluis-Miquel Munguia Conejero (lluis.munguia@gatech.edu)
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/ 
// ----------------------------------------------------------------------------
#ifndef __SOLVER_H
#define __SOLVER_H

#include <iostream> 
#include <vector>
#include <sstream>
#include <algorithm>
#include <random>
#include <omp.h>
#include <mpi.h>
#include <cplex.h>
#include "Solution.h"


using namespace std;


#define PRINTOUT 0
#define PRINTOUTCPLEX 0
#define EPSILON 1e-6
#define COVER_CHANGE -0.05
#define TIME_CHANGE 10
#define VIOLATED_CONSTR_THRESHOLD 200
#define VIOLATED_VARS_THRESHOLD 100

class solver{ 

public:
  //construction and initialization
  solver(); 
  ~solver();

  int initSolver(string &s, int thread,double &_TOTAL_CONSTRS_COVER,int &_INITIAL_TILIM,double &_TOTAL_CONSTRS_COVER_OBJ,int &_INITIAL_TILIM_OBJ, double &_DELTA_TOLERANCE, double &_INIT_FIXING_COVER, int &NTHREADS, int &nproc);
  int initSolverAsCopy(CPXLPptr *_lp, int thread,double _TOTAL_CONSTRS_COVER,int _INITIAL_TILIM, double _TOTAL_CONSTRS_COVER_OBJ,int _INITIAL_TILIM_OBJ,double _DELTA_TOLERANCE, double _INIT_FIXING_COVER, int NTHREADS, int nproc);
  void createProblemStructures(); 
  void setStartingTimestamp(double time);

  //accessor/setter methods
  int getNVars();
  void changeObjTimeLim(double change);
  void changeDeltaTimeLim(double change);
  void changeObjCoverage(double Change);
  void changeDeltaCoverage(double change);

  CPXLPptr* getOriginalProbLP(){
    return &originalLp;
  }
  //solving methods
  void solveRelaxed(vector<double> &retvals, double &objVal, int &fracVals, int &relaxationIterations);
  int performIteration(Solution &inSol, Solution &outSol, int iter,int nthread);
  int performObjectiveImprovementIteration(Solution &inSol, Solution &outSol, int iter,int nthread);
  int compactSolutionsForObj(vector<Solution> &Sols, Solution &outSol, int iter);
  int compactSolutionsForDelta(vector<Solution> &DeltaSols, Solution &outSol, int iter);



private:

  //Auxiliary functions
  int collectInconsistencies(vector<double> &MIPvarArr, vector<int> &inconsistentIndices, vector<double> &slacks);

  //environment and problem data structures
  CPXLPptr originalLp;
  CPXENVptr env;
  CPXLPptr lp; 
  CPXLPptr objLp;

  //Parallel threads / time info
  int nproc;
  int NUM_THREADS;
  int CPLEXTHREADS;
  double startingTimestamp;
  
  //Search parameters
  double TOTAL_CONSTRS_COVER;
  double TOTAL_CONSTRS_COVER_OBJ; 
  int INITIAL_TILIM;
  double DELTA_TOLERANCE;
  double INIT_FIXING_COVER;
  int tilim;
  int tilimObj;

  //Model data
  double LPRelaxationValue;
  vector<int>* integerVariableIndices;
  int numVars;
  int numConsts;
  int numberOfMaxConsts;
  int numberOfNonZeroCoefs;

  vector<double> constRHS;  
  vector<char> constSenses;  
  vector<int> constBeginning;
  vector<int> constIndices;
  vector<double> constCoefs;
  vector<char> varTypes;
  vector<double> variableObjectiveCoefficients;
  vector<double> maxVarsObjectiveCoefficients;
  vector<double> MaxConstsRHS;
  vector<int> MaxConstsBeginning;
  vector<int> MaxConstsIndices;
  vector<double> MaxConstsCoefs;
  vector<char> MaxConstsSenses;
  vector<char *> MaxConstsNames;
  vector<double> savedVariableLowerBounds;
  vector<double> savedVariableUpperBounds;
  vector<double> savedDeltaLowerBounds;
  vector<double> savedDeltaUpperBounds;


};


#endif
