// ----------------------------------------------------------------------------
/**
   File: Solution.h

   Description: Container class for candidate integer solutions. 
   Created by Lluis-Miquel Munguia Conejero (lluis.munguia@gatech.edu)
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the IMPLIED WARRANTY OF
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/ 
// ----------------------------------------------------------------------------
#ifndef __SOLUTION_H
#define __SOLUTION_H

#include <vector>

using namespace std;

class Solution{ 

public:
	//construction and initialization
	Solution();	
	~Solution();
	int initSolution(double *serializedSolution);
	int initSolution(vector<double> &_values, double _fracVals, double _solVal,int iteration, double cutOffDeltaRHS, double discoveryTime);

	//accessor methods
	double getValue() const;
	double getLB() const;
	double getFracVals() const;
	void getValueVector(vector<double> &values);
	double getcutOffDeltaRHS();

	//serialization methods
	void serializeSolution(vector<double> &serializedSolution);
	int getSerialSize();
	double getDiscoveryTime();
private:
	vector<double> values;
	double fracVals;
	double solVal;
	int iteration;
	double cutOffDeltaRHS;
	double discoveryTime;
	
};



#endif
